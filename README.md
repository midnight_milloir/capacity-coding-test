
# Python Version: **3.9.4**

Make sure a version of the python3 binary is on your path!

## Setting up the Environment

To install the required packages for this project run the following commands:


1. Optional: `python3 -m venv ~/path/to/new/virtual-environment`
2. Optional: `source ~/path/to/new/virtual-environment/bin/activate`
3. `pip install -r requirements.txt`

## The capacity app

Run the application with the following command on a UNIX System:

- `python capacity.py` - Starts the capacity application. Follow the prompts to explore the features of the application.

## Database

You will need to install and run mysql. Afterwards you will need to create the user, database, and give db privileges to the user you just created.

## Note:
Please update your .env file with the appropriate values for the Database User, Database Password, and Database Name
