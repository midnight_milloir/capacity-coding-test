#!/usr/bin/env python3
from pathlib import Path

from lib.database.cli import DatabaseDesign
from lib.api.cli import ApiInteractions
from lib.algorithms.cli import Algorithms

def main():
  option = 0
  
  while(option != '4'):
    print("Select an option: \n1 - Database Design\n2 - Api Interaction\n3 - Algorithms\n4 - Exit\n")
    option = input("option: ")
  
    if option == '1':
      DatabaseDesign()
  
    elif option == '2':
      ApiInteractions()
  
    elif option == '3':
      Algorithms()
  
    elif option == '4':
      print("\n\nThanks for playing...\n\n")
  
    else:
      print("Invalid option. Try again.")

if __name__ == '__main__':
  main()
