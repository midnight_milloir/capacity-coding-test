#!/usr/bin/env python3
from itertools import permutations


def Algorithms():
  filepath = input("Enter path/to/file: ")

  with open(filepath, "rt") as f:
    input_data = [x for x in f.read().split('\n') if x]

  print('Found {} lines of input data'.format(len(input_data)))
  permutations = map(get_permutations, input_data)

  print('\n'.join(permutations))

def get_permutations(other):
  results = []
  strings = permutations(other)
  results = map(''.join, strings)
  return ','.join(sorted(results))
