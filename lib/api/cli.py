#!/usr/bin/env python3
from pathlib import Path
import requests
from .controllers.todo import ToDoController

def ApiInteractions():
  todo_controller = ToDoController()

  option = 0
  
  while(option != '4'):
    print("Select an option: \n1 - Get\n2 - Create\n3 - Delete\n4 - Back\n")
    option = input("option: ")
  
    if option == '1':
      todos = todo_controller.get_todos()
      if not todos:
        print('Failed to return TODO data from API...')
        return
      display_option = input("Display todos? [y/n]: ")
      if display_option == 'y':
        print(todos)

    elif option == '2':
      todo_controller.create_todo()
  
    elif option == '3':
     todo_controller.delete_todo()
    
    elif option == '4':
      pass
  
    else:
      print("Invalid option. Try again.")
