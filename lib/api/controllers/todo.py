from pathlib import Path
import requests
import json

class ToDoController:
  BASE_API_URL = 'http://jsonplaceholder.typicode.com'
  TODO_ENDPOINT = '{}/todos'.format(BASE_API_URL)
  REQUEST_HEADERS = {'Content-Type': 'application/json; charset=UTF-8'}

  def _validate_response(self, response):
    if response.status_code in (200, 201):
      print('{r.request.method} Response to {r.url} returned {r.status_code}'.format(r=response))
      return json.loads(response.content)

    elif response.status_code != 200:
      print('{r.request.method} response returned to {r.url}: {r.status_code}'.format(r=response))
      return
  

  def get_todos(self):
    response = requests.get(self.TODO_ENDPOINT)
    todos = self._validate_response(response)

    if not todos:
      return

    print('Found {} TODOs'.format(len(todos)))
    return todos


  def delete_todo(self):
    valid_id = input("Enter your TODO ID: ")
    delete_todo_endpoint = '{}/{id}'.format(self.TODO_ENDPOINT, id=valid_id)
    response = self._validate_response(requests.delete(delete_todo_endpoint))


  def create_todo(self):
    user_id = input('Enter your User ID: ')
    todo_title = input('Enter the TODO title: ')
    todo_content = input('Enter the body of your TODO: ')

    todo_obj = json.dumps({
      'userId': user_id,
      'title': todo_title,
      'body': todo_content
    })

    response = requests.post(self.TODO_ENDPOINT, data=todo_obj, headers=self.REQUEST_HEADERS)
    response_payload = self._validate_response(response)

    if not response_payload:
      return

    print('New TODO Created w/ ID: {r[id]}'.format(r=response_payload))
    return response_payload

