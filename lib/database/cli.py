from .gateway import CapacityGateway

def DatabaseDesign():
  print("connecting to database...")
  db = CapacityGateway()
  db.init_db()

  option = 0

  while(option != '4'):
    print("Select an option: \n1 - User\n2 - Post\n3 - Comment\n4 - Back\n")
    option = input("option: ")

    if option == '1':
      print("Select an option: \n1 - List all users\n2 - Find user\n3 - Create\n4 - Update\n5 - Delete\n")
      user = input("option: ")
      if user == '1':
        db.query("User")
      elif user == '2':
        username = input("username: ")
        results = db.query("User", [username])
      elif user == '3':
        db.insert("User", UserForm())
      elif user == '4':
        db.update("User", UserForm())
      elif user == '5':
        username = input("username: ")
        db.delete("User", [username])
      else:
        print("Invalid option. Try again.")
      
    elif option == '2':
      print("Select an option: \n1 - List all posts for a specified user\n2 - Find post\n3 - Create\n4 - Update\n5 - Delete\n")
      post = input("option: ")
      if post == '1':
        username = input("username: ")
        db.query("Post", [username])
      elif post == '2':
        username = input("username: ")
        title = input("title: ")
        results = db.query("Post", [username, title])
      elif post == '3':
        db.insert("Post", PostForm())
      elif post == '4':
        db.update("Post", PostForm())
      elif post == '5':
        username = input("username: ")
        title = input("title: ")
        db.delete("Post", [username, title])
      else:
        print("Invalid option. Try again.")

    elif option == '3':
      print("Select an option: \n1 - List all comments for a specified user\n2 - Create\n3 - Update\n4 - Delete\n")
      comment = input("option: ")
      if comment == '1':
        username = input("username: ")
        db.query("Comment", [username])
      elif comment == '2':
        db.insert("Comment", CommentForm())
      elif comment == '3':
        db.update("Comment", CommentForm())
      elif comment == '4':
        username = input("username: ")
        title = input("title of post: ")
        db.query("Comment", [username, title])
        db.display_results()
        comment_id = input("enter the id of the comment you wish to delete: ")
        db.delete("Comment", [comment_id])
      else:
        print("Invalid option. Try again.")
    
    elif option == '4':
      pass

    else:
      print("Invalid option. Try again.")

    if db.errors:
      print("There were errors", db.errors)
      db.errors.clear()
      return
    else:
      db.display_results()
      db.results = []
    
def UserForm():
  username = input("username: ")
  password = input("password: ")
  email = input("email: ")
  website = input("personal website: ")
  return [username, password, email, website]

def PostForm():
  username = input("username: ")
  title = input("title: ")
  slug = input("slug: ")
  body = input("body : ")
  published = input("published? [y/n]: ")
  if published == 'y':
    published = True
  else:
    published = False
  likes = input("number of likes: ")
  return [username, title, slug, body, published, likes]

def CommentForm():
  username = input("username: ")
  title = input("title: ")
  comment = input("comment: ")
  review = input("needs review? [y/n] : ")
  if review == 'y':
    review = True
  else:
    review = False
  likes = input("number of likes: ")
  return [username, title, comment, review, likes]
