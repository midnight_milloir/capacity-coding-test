import os
from peewee import *
import sys
from dotenv import load_dotenv
from .uow.useruow import UserUoW
from .uow.postuow import PostUoW
from .uow.commentuow import CommentUoW
from .models import *
import sys

load_dotenv(verbose=True)

DATABASE_USER = os.getenv('DATABASE_USER')
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD')
DATABASE_NAME = os.getenv('DATABASE_NAME')
INIT_SQL = '''
CREATE USER IF NOT EXISTS '{user}'@'localhost'
  IDENTIFIED WITH sha256_password BY '{passw}'
  PASSWORD EXPIRE INTERVAL 180 DAY;

CREATE DATABASE IF NOT EXISTS {database};

GRANT ALL PRIVILEGES ON {database}.* TO '{user}'@'localhost';
'''.format(
  user=DATABASE_USER,
  passw=DATABASE_PASSWORD,
  database=DATABASE_NAME,
)

class CapacityGateway:
  db = MySQLDatabase
  userUoW = UserUoW()
  postUoW = PostUoW()
  commentUoW = CommentUoW()
  results = []
  errors = []

  def init_db(self):
    try:
      self.db = MySQLDatabase(DATABASE_NAME, user=DATABASE_USER, password=DATABASE_PASSWORD)
      self.db.connect()
      self.db.create_tables([user.User, post.Post, comment.Comment])
    except (InterfaceError, OperationalError):
      self.errors.append('Unable to connect to database. Run the following sql in a mysql client of your choosing -> ' + INIT_SQL)
  

  def query(self, table_name, params = []):
    # Selects data from database for a defined table & params

    if table_name == "User":
      if not params:
        self.results = self.db.execute(self.userUoW.get_all())
      else:
        self.results = self.db.execute(self.userUoW.find(params[0]))

    elif table_name == "Post":
      if params:
        user = self.db.execute(self.userUoW.get_id(params[0])).fetchone()
        if not user:
          self.errors.append("User does not exist")
          return
        if len(params) == 1:
          self.results = self.db.execute(self.postUoW.get_all(user[0]))
        else:
          self.results = self.db.execute(self.postUoW.find(user[0], params[1]))
    
    elif table_name == "Comment":
      if params:
        if len(params) == 2:
          user = self.db.execute(self.userUoW.get_id(params[0])).fetchone()
          if not user:
            self.errors.append("User does not exist")
            return
          post = self.db.execute(self.postUoW.get_id(user[0], params[1])).fetchone()
          if not post:
            self.errors.append("Post does not exist")
            return
          self.results = self.db.execute(self.commentUoW.get_all(post[0], user[0]))
        else:
          user = self.db.execute(self.userUoW.get_id(params[0])).fetchone()
          if not user:
            self.errors.append("User does not exist")
            return
          self.results = self.db.execute(self.commentUoW.get_all_user(user[0]))
  


  def insert(self, table_name, params = []):
    # Inserts params into the database for a defined table
    if not params:
      print("No params given")
      return

    
    if table_name == "User":
      self.results = self.db.execute(self.userUoW.insert(params[0], params[1], params[2], params[3]))
    
    elif table_name == "Post":
      user = self.db.execute(self.userUoW.get_id(params[0])).fetchone()
      if not user:
        self.errors.append("User does not exist")
        return
      self.results = self.db.execute(self.postUoW.insert(user[0], params[1], params[2], params[3], params[4], params[5]))
    
    elif table_name == "Comment":
      user = self.db.execute(self.userUoW.get_id(params[0])).fetchone()
      if not user:
        self.errors.append("User does not exist")
        return
      post = self.db.execute(self.postUoW.get_id(user[0], params[1])).fetchone()
      if not post:
        self.errors.append("Post does not exist")
        return
      self.results = self.db.execute(self.commentUoW.insert(post[0], user[0], params[2], params[3], params[4]))
    
  
 
  def update(self, table_name, params = []):
    # Updates params in the database for a defined table
    if not params:
      print("No params given")
      return
    
    try:
    
      if table_name == "User":
        self.results = self.db.execute(self.userUoW.update(params[0], params[1], params[2], params[3]))
    
      elif table_name == "Post":
        user = self.db.execute(self.userUoW.get_id(params[0])).fetchone()
        if not user:
          self.errors.append("User does not exist")
          return
        self.results = self.db.execute(self.postUoW.update(user[0], params[1], params[2], params[3], params[4], params[5]))
    
      elif table_name == "Comment":
        user = self.db.execute(self.userUoW.get_id(params[0])).fetchone()
        if not user:
          self.errors.append("User does not exist")
          return
        post = self.db.execute(self.postUoW.get_id(user[0], params[1])).fetchone()
        if not post:
          self.errors.append("Post does not exist")
          return
        self.results = self.db.execute(self.commentUoW.update(post[0], user[0], params[2], params[3], params[4]))
    
    except (InterfaceError, OperationalError):
      self.errors.append("Update Failure")


  def delete(self, table_name, params = []):
    # Deteles rows from the database for a defined table
    if not params:
      print("No params given")
      return

    try:

      if table_name == "User":
        self.results = self.db.execute(self.userUoW.delete(params[0]))

      elif table_name == "Post":
        user = self.db.execute(self.userUoW.get_id(params[0])).fetchone()
        if not user:
          self.errors.append("User does not exist")
          return
        self.results = self.db.execute(self.postUoW.delete(user[0], params[1]))

      elif table_name == "Comment":
        self.results = self.db.execute(self.commentUoW.delete(params[0]))

    except (InterfaceError, OperationalError):
      self.errors.append("Delete Failure")
  

  def display_results(self):
    if self.results:
      for content in self.results:
        print('->', content)
