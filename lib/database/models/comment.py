import datetime
import os
from peewee import *
import sys
from dotenv import load_dotenv
from .user import User
from .post import Post

load_dotenv(verbose=True)

DATABASE_USER = os.getenv('DATABASE_USER')
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD')
DATABASE_NAME = os.getenv('DATABASE_NAME')

try:
  db = MySQLDatabase(DATABASE_NAME, user=DATABASE_USER, password=DATABASE_PASSWORD)
  db.connect()
except (InterfaceError, OperationalError):
  print('Unable to connect to database')

class Comment(Model):
  _id = AutoField(primary_key=True)
  post_id = ForeignKeyField(Post, backref='posted_on')  # Link Comments to Posts
  created_by = ForeignKeyField(User, backref='user')  # Link Users to Comments
  created_at = TimestampField(null=False, default=datetime.datetime.now)
  comment = TextField(null=False)
  needs_review = BooleanField(null=False, default=True)  # Require content moderation by default
  likes = IntegerField()

  class Meta:
    database = db
