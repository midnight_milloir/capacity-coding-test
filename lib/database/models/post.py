import datetime
import os
from peewee import *
from dotenv import load_dotenv
from .user import User

load_dotenv(verbose=True)

DATABASE_USER = os.getenv('DATABASE_USER')
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD')
DATABASE_NAME = os.getenv('DATABASE_NAME')

try:
  db = MySQLDatabase(DATABASE_NAME, user=DATABASE_USER, password=DATABASE_PASSWORD)
  db.connect()
except (InterfaceError, OperationalError):
  print('Unable to connect to database')

class Post(Model):
  _id = AutoField(primary_key=True)
  created_by = ForeignKeyField(User, backref='posts')  # Link Posts to Users
  created_at = TimestampField(null=False, default=datetime.datetime.now, index=True)
  title = CharField(null=False)
  slug = CharField(unique=True)  # Web URL Slug this-is-a-post
  body = TextField(null=False)
  published = BooleanField(default=False, null=False, index=True)  # Allow for Drafts with a simple flag
  likes = IntegerField()

  class Meta:
    database = db
