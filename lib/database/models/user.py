import os
from peewee import *
from dotenv import load_dotenv

load_dotenv(verbose=True)

DATABASE_USER = os.getenv('DATABASE_USER')
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD')
DATABASE_NAME = os.getenv('DATABASE_NAME')

try:
  db = MySQLDatabase(DATABASE_NAME, user=DATABASE_USER, password=DATABASE_PASSWORD)
  db.connect()
except (InterfaceError, OperationalError):
  print('Unable to connect to database')

class User(Model):
  _id = AutoField(primary_key=True)
  username = CharField(unique=True, null=False, max_length=25)
  password = CharField(null=False, max_length=25)
  email = CharField(null=False, max_length=35)
  personal_website = CharField(max_length=75)

  class Meta:
    database = db
