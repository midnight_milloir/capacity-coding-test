from peewee import *
from ..models.comment import Comment

class CommentUoW:

  def get_all_user(self, created_by):
    return Comment.select().where(Comment.created_by == created_by).order_by(Comment.created_at)

  def get_all(self, post_id, created_by):
    return Comment.select().where(Comment.post_id == post_id, Comment.created_by == created_by).order_by(Comment.created_at)
  
  def insert(self, post_id, created_by, comment, needs_review=False, likes=0):
    return Comment.insert(post_id=post_id, created_by=created_by, comment=comment, needs_review=needs_review, likes=likes)
  
  def update(self, post_id, created_by, comment, needs_review=False, likes=0):
    return Comment.update(comment=comment, needs_review=needs_review, likes=likes).where(Comment.post_id == post_id, Comment.created_by == created_by)
  
  def delete(self, comment_id):
    return Comment.delete().where(Comment._id == comment_id)
