from peewee import *
from ..models.post import Post

class PostUoW:

  def get_id(self, created_by, title):
    return Post.select(Post._id).where(Post.created_by == created_by, Post.title == title)

  def get_all(self, created_by):
    return Post.select().where(Post.created_by == created_by).order_by(Post.title)
  
  def find(self, created_by, title):
    return Post.select().where(Post.created_by == created_by, Post.title == title)

  def insert(self, created_by, title, slug, body, published = False, likes=0):
    return Post.insert(created_by=created_by, title=title, slug=slug, body=body, published=published, likes=likes)
  
  def update(self, created_by, title, slug, body, published=False, likes=0):
    return Post.update(body=body, published=published, likes=likes).where(Post.created_by == created_by, Post.title == title)
  
  def delete(self, created_by, title):
    return Post.delete().where(Post.created_by == created_by, Post.title == title)