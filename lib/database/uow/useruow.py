from peewee import *
from ..models.user import User

class UserUoW:

  def get_id(self, username):
    return User.select(User._id).where(User.username == username)

  def get_all(self):
    return User.select().order_by(User.username)
  
  def find(self, username):
    return User.select().where(User.username == username)

  def insert(self, username, password, email, personal_website):
    return User.insert(username=username, password=password, email=email, personal_website=personal_website)
  
  def update(self, username, password, email, personal_website):
    return User.update(password=password, email=email, personal_website=personal_website).where(User.username == username)
  
  def delete(self, username):
    return User.delete().where(User.username == username)